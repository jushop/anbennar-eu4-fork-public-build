government = imperial_city
government_rank = 1
mercantilism = 25
primary_culture = arannese
religion = regent_court
technology_group = tech_cannorian
capital = 415 #Tellum
national_focus = DIP

1440.1.1 = {
	monarch = {
		name = "City Council"
		adm = 2
		dip = 2
		mil = 2
		regent = yes
	}
}