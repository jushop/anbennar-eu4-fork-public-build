government = magisterium
government_rank = 1
primary_culture = east_damerian
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 3 #Old Damenath

elector = yes

1437.9.1 = {
	monarch = {
		name = "Crovan"
		dynasty = "of Vinerick"
		culture = wexonard
		birth_date = 1386.4.12
		adm = 2
		dip = 4
		mil = 5
	}
}