#371 - Akureyri

owner = A29
controller = A29
add_core = A29
culture = busilari
religion = regent_court

hre = no

base_tax = 5
base_production = 5
base_manpower = 3

trade_goods = copper

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy