#Tr�ndelag, incl. Trondheim, Frosta, R�ros, Steinviksholm

owner = A01
controller = A01
add_core = A01
culture = west_damerian
religion = regent_court
hre = yes
base_tax = 3
base_production = 5
trade_goods = grain
base_manpower = 4
capital = "" 
is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
