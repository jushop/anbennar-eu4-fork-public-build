#454 - Samarkand

owner = F10
controller = F10
add_core = F10
culture = kheteratan
religion = kheteratan_pantheon

hre = no

base_tax = 3
base_production = 4
base_manpower = 3

trade_goods = livestock

capital = ""

is_city = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = ottoman

