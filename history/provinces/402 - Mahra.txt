#402 - Qishn

owner = F08
controller = F08
add_core = F08
culture = far_akani
religion = kheteratan_pantheon

hre = no

base_tax = 6
base_production = 3
base_manpower = 3

trade_goods = cloth

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish

add_permanent_province_modifier = {
	name = center_of_trade_modifier
	duration = -1
}