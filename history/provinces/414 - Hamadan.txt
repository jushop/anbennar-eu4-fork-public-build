#414 - Esssilvar

owner = A56
controller = A56
add_core = A56
culture = arannese
religion = regent_court

hre = yes

base_tax = 3
base_production = 3
base_manpower = 3

trade_goods = wool

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy

