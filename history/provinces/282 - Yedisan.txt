# No previous file for Byshade
owner = A98
controller = A98
add_core = A98
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 3
base_production = 3
base_manpower = 2

trade_goods = livestock

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish