#79 - Wurzburg | Eil�sin

owner = A15 #Duchy of Eil�sin
controller = A15
add_core = A15
culture = high_lorentish
religion = regent_court

hre = no

base_tax = 7
base_production = 7
base_manpower = 7

trade_goods = wine

capital = "The Twin Eil�s"

is_city = yes
fort_15th = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

add_permanent_province_modifier = {
	name = elven_minority_integrated_large
	duration = -1
}