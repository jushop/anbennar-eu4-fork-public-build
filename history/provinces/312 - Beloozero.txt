#312 - Beloozero

owner = A75
controller = A75
add_core = A75
culture = arannese
religion = regent_court

hre = yes

base_tax = 4
base_production = 2
base_manpower = 2

trade_goods = livestock

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
