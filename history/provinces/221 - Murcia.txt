#221 - Murcia | Gaweston

owner = A13
controller = A13
add_core = A13
culture = gawedi
religion = regent_court

hre = no

base_tax = 6
base_production = 6
base_manpower = 4

trade_goods = fur

capital = ""

is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
discovered_by = tech_orcish


add_permanent_province_modifier = {
	name = inland_center_of_trade_modifier
	duration = -1
}

add_permanent_province_modifier = {
	name = halfling_minority_oppressed_large
	duration = -1
}