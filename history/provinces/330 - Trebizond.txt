#330 - Trebizond

owner = A30
controller = A30
#add_core = A30
add_core = A45
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 6
base_production = 6
base_manpower = 4

trade_goods = cloth

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish

1443.11.11 = {	
	owner = A30
	controller = A30
}