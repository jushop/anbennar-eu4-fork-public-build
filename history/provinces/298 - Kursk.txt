# No previous file for Kursk
owner = A98
controller = A98
add_core = A98
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 4
base_production = 4
base_manpower = 3

trade_goods = grain

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish