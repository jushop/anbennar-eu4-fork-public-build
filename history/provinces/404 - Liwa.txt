#404 - Liwa

owner = F06
controller = F06
add_core = F06
culture = desha
religion = kheteratan_pantheon

hre = no

base_tax = 5
base_production = 5
base_manpower = 5

trade_goods = cloth
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
