#216 - Salamanca | 

owner = A23
controller = A23
add_core = A23
culture = vertesker
religion = regent_court

hre = yes

base_tax = 10
base_production = 13
base_manpower = 10

trade_goods = slaves

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold
discovered_by = tech_orcish

add_permanent_province_modifier = {
	name = center_of_trade_modifier
	duration = -1
}

add_permanent_province_modifier = {
	name = alen_estuary_modifier
	duration = -1
}

add_permanent_province_modifier = {
	name = castanorian_citadel
	duration = -1
}

add_permanent_province_modifier = {
	name = halfling_minority_oppressed_large
	duration = -1
}