#479 - Kerey

owner = F01
controller = F01
add_core = F01
culture = kheteratan
religion = kheteratan_pantheon

hre = no

base_tax = 4
base_production = 4
base_manpower = 3

trade_goods = gold

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy