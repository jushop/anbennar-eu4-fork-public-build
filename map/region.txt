# A province can only belong to one region!

# random_new_world_region is used for RNW. Must be first in this list.
random_new_world_region = {
}

########################################
# WESTERN CANNOR                       #
########################################

lencenor_region = {
	areas = {
		venail_area
		upper_bloodwine_area
		lower_bloodwine_area
		sorncost_vines_area
		sornhills_area
		shrouded_coast_area
		ionnidar_area
		redglades_area
		ruby_mountains_area
		deranne_area
		northern_flats_area
		southern_flats_area
		darom_area
		eastern_winebay_area
		upper_winebay_area
		lower_winebay_area
		southroy_area
	}
}

small_country_region = {
	areas = {
		viswall_area
		roysfort_area
		the_borders_area
		thomsbridge_area
		applefields_area
		lorenans_reach_area
		lorentish_approach_area
		pearpoint_area
		norley_area
	}
}

dragon_coast_region = {
	areas = {
		gnomish_pass_area
		nimscodd_area
		dragonpoint_area
		dragondowns_area
		dragonspine_area
		dragonheights_area
		storm_isles_area
		iochand_area
		reaver_coast_area
		dragonhills_area
	}
}

west_dameshead_region = {
	areas = {
		wesdam_area
		neckcliffe_area
		damespearl_area
		pearlywine_area
		ilvandet_area
		area15_area
		area16_area
		exwes_area
		woodwell_area
	}
}

east_dameshead_region = {
	areas = {
		west_damesear_area
		east_damesear_area
		eastneck_area
		damerian_dales_area
		silverwoods_area
		area7_area
		heartlands_area
		area8_area
		cestirande_area
		area17_area
		verne_area
		galeinn_area
	}
}

the_borders_region = {
	areas = {
		wexhills_area
		bisan_area
		area9a_area
		area9b_area
		area13_area
		gnollsgate_area
		area12_area
		arannen_area
		area2_area
		area1_area
		area3a_area
		area3b_area
	}
}

forlorn_vale_region = {
	areas = {
		north_ibevar_area
		south_ibevar_area
		silent_repose_area
		area19_area
		area20_area
		area23_area
		rotwall_area
		area31_area
	}
}

esmaria_region = {
	areas = {
		konwell_area
		high_esmar_area
		low_esmar_area
		area14_area
		bennonhill_area
		cann_esmar_area
		area11_area
		ashfields_area
	}
}

businor_region = {
	areas = {
		lorbet_area
		mountainway_area
		busilar_area
		busilari_straits_area
		area5_area
		khenak_area
		isle_of_tef_area
	}
}

alenic_frontier_region = {
	areas = {
		westmoor_proper_area
		moorhills_area
		beronmoor_area
		area18_area
		alenic_expanse_area
		gawed_area
		south_alen_area
		alenvord_area
		balvord_area
		area22_area
		arbaran_area
		golden_plains_area
		cestir_area
		storm_coast_area
	}
}

damescrown_region = {
	areas = {
		damescrown_area
		floodmarches_area
		vertesk_area
		dames_forehead_area
		beepeck_area
		derwing_area
		heartland_borders_area
	}
}

dostanor_region = {
	areas = {
		corvurian_plains_area
		baldostan_area
		central_corvuria_area
		blackwoods_area
	}
}

daravans_folly_region = {
	areas = {
		dreadmire_area
		flooded_coast_area
	}
}

inner_castanor_region = {
	areas = {
		castonath_area
		upper_nath_area
		lower_nath_area
		westgate_area
		southgate_area
		trialmount_area
		steelhyl_area
		area68_area
		area71_area
		area72_area
		area73_area
		area74_area
		area75_area
		area76_area
		area77_area
		area78_area
		area80_area
		area84_area
	}
}

south_castanor_region = {
	areas = {
		burnoll_area
		dostans_way_area
		area53_area
		area54_area
		area55_area
		area56_area
		area57_area
		area58_area
		area59_area
		area60_area
		area61_area
		area62_area
		area63_area
		area64_area
		area65_area
		area66_area
		area81_area
		area82_area
		area83_area
	}
}

west_castanor_region = {
	areas = {
		balmire_area
		westwall_approach_area
		area40_area
		area41_area
		area42_area
		area43_area
		area44_area
		area45_area
		area46_area
		area47_area
		area48_area
		area49_area
		area50a_area
		area50b_area
		area51_area
		area52_area
	}
}


akan_region = {
	areas = {
		deshak_area
		ekha_area
		middle_akan_area
		east_akan_area
		central_akan_area
		west_akan_area
	}
}

north_salahad_region = {
	areas = {
		area32_area
		area33_area
		area34_area
		coast_of_tears_area
		mothers_delta_area
		area35_area
		lower_sorrow_area
		kheterat_proper_area
		upper_sorrow_area
		area37_area
		area38_area
		elizna_area
		area39_area
	}
}

gol_region = {
	areas = {
		golkora_stretch_area
		gnollakaz_area
		area36_area
	}
}

#Sea Regions
ueloss_lament_region = {
	areas = {
		divengate_sea_area
		dameshead_sea_area
	}
}

dameshead_sea_region = {
	areas = {
		damesneck_sea_area
		dameshead_sea_area
	}
}

giants_grave_sea_region = {
	areas = {
		giants_grave_sea_area
		bay_of_chills_area
	}
}

divenhal_sea_region = {
	areas = {
		bay_of_wines_sea_area
		west_diven_area
		coast_of_akan_area
		east_diven_area
		sea_of_follies_area
		sea_of_stone_area
		gulf_of_glass_area
		brasanni_sea_area
	}
}

westcoast_region = {
	areas = {
		southcoast_area
		westcoast_area
		bay_of_dragons_area
	}
}

northern_thaw_region = {
	areas = {
		reaversea_area
	}
}
