# Do not change tags in here without changing every other reference to them.
# If adding new dieties, make sure they are uniquely named.

castellos =
{
	legitimacy = 0.1
	horde_unity = 0.1
	global_unrest = -1
	
    allow = { 
        OR = {
                religion = cannorian_pantheon
                religion = regent_court
        }
    }
	
	sprite = 7
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			NOT = { num_of_cities = 5 }
		}
		modifier = {
			factor = 2
			NOT = { num_of_cities = 10 }
		}
		modifier = {
			factor = 0.5
			num_of_cities = 30
		}
		modifier = {
			factor = 0.5
			personality = ai_diplomat
		}
	}
}

the_dame =
{
	production_efficiency = 0.1
	idea_cost = -0.1
	
    allow = { 
        OR = {
                religion = cannorian_pantheon
                religion = regent_court
        }
    }
	
	sprite = 8
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			personality = ai_balanced
		}	
		modifier = {
			factor = 0.5
			personality = ai_colonialist
		}
	}
}

corin =
{
	leader_land_shock = 1
	land_morale = 0.05
	
    allow = { 
        OR = {
                religion = cannorian_pantheon
                religion = regent_court
        }
    }
	sprite = 9
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			NOT = { num_of_cities = 5 }
		}
		modifier = {
			factor = 2
			NOT = { num_of_cities = 10 }
		}
		modifier = {
			factor = 0.5
			num_of_cities = 30
		}
		modifier = {
			factor = 2
			personality = ai_militarist
		}	
		modifier = {
			factor = 0.5
			personality = ai_capitalist
		}		
	}
}

adean =
{
	diplomatic_reputation = 1
	discipline = 0.05
	
    allow = { 
        OR = {
                religion = cannorian_pantheon
                religion = regent_court
        }
    }
	sprite = 10
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			personality = ai_balanced
		}	
		modifier = {
			factor = 0.5
			personality = ai_colonialist
		}		
	}
}

munas_moonsinger =
{
	naval_morale = 0.1
	improve_relation_modifier = 0.33
	
    allow = { 
        OR = {
                religion = cannorian_pantheon
                religion = regent_court
        }
    }
	sprite = 10
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			personality = ai_balanced
		}	
		modifier = {
			factor = 0.5
			personality = ai_colonialist
		}		
	}
}

ara =
{
	trade_efficiency = 0.1
	global_tax_modifier = 0.1
	
    allow = { 
        OR = {
                religion = cannorian_pantheon
                religion = regent_court
        }
    }
	sprite = 11
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			personality = ai_capitalist
		}	
		modifier = {
			factor = 2
			personality = ai_colonialist
		}	
		modifier = {
			factor = 0.5
			personality = ai_militarist
		}		
	}
}


balgar =
{
	build_cost = -0.1
	defensiveness = 0.1
	
    allow = { 
        OR = {
                religion = cannorian_pantheon
                religion = regent_court
        }
    }
	sprite = 12
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			personality = ai_diplomat
		}	
		modifier = {
			factor = 0.5
			personality = ai_militarist
		}		
	}
}

edronias =
{
	land_attrition = -0.10
	manpower_recovery_speed = 0.1	
	
    allow = { 
        OR = {
                religion = cannorian_pantheon
                religion = regent_court
        }
    }
	sprite = 12
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 1
			personality = ai_diplomat
		}	
		modifier = {
			factor = 1
			personality = ai_militarist
		}		
	}
}

esmaryal =
{
	heir_chance = 0.5
	war_exhaustion = -0.02
	
    allow = { 
        OR = {
                religion = cannorian_pantheon
                religion = regent_court
        }
    }
	sprite = 12
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			personality = ai_diplomat
		}	
		modifier = {
			factor = 1
			personality = ai_militarist
		}		
	}
}


ryala =
{
	prestige = 1
	tolerance_heathen = 2
	
    allow = { 
        OR = {
                religion = cannorian_pantheon
                religion = regent_court
        }
    }
	sprite = 12
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 1
			personality = ai_diplomat
		}	
		modifier = {
			factor = 1
			personality = ai_militarist
		}		
	}
}

minara =
{
	ae_impact = -0.2
	diplomatic_upkeep = 1
	
    allow = { 
        OR = {
                religion = cannorian_pantheon
                religion = regent_court
        }
    }
	sprite = 12
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 1
			personality = ai_diplomat
		}	
		modifier = {
			factor = 2
			personality = ai_militarist
		}		
	}
}

nerat =
{
	global_autonomy = -0.05
	hostile_attrition = 1
	
    allow = { 
        OR = {
                religion = cannorian_pantheon
                religion = regent_court
        }
    }
	sprite = 12
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 1
			personality = ai_diplomat
		}	
		modifier = {
			factor = 2
			personality = ai_militarist
		}		
	}
}

